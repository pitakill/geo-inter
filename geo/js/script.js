//Hay que agregar un Namespace; aunque me temo que se tendrán que rehacer algunas cosas.
'use strict';

//Todo lo relacionado con la clase Canvas.
var Canvas = function(canvasId) {
  this.scene = document.getElementById(canvasId) || document.getElementsByTagName('canvas')[0];
  this.sceneContext = this.scene.getContext('2d');
  this.event;
};
//Métodos.
Canvas.prototype = {
  drawPoint : function(Point) {
    var context = this.sceneContext;
    context.beginPath();
    context.arc(Point.x, Point.y, Point.dimension, 0, 2 * Math.PI);
    context.fillStyle = Point.color;
    context.lineWidth = Point.dimension / 2;
    context.strokeStyle = Point.color + 500;
    context.lineCap = 'round';
    context.stroke();
    context.fill();
    return this;
  },
  listen : function() {
    this.scene.addEventListener('click', this.seHizoClick());
  },
  seHizoClick : function() {
    event.preventDefault();
    console.log(event);
    var X, Y;
    X = event.layerX;
    Y = event.layerY;
    event.srcElement.drawPoint(new Point('green', 10, X, Y));
  }
};
// Todo lo relacionado con los objetos Point.
var Point = function(color, dimension, x, y) {
  this.color = color || 'black';
  this.dimension = dimension || 5;
  this.x = x || 25;
  this.y = y || 25;
};
//Métodos.
Point.prototype = {
  setColor : function(color) {
    this.color = color;
    return this;
  },
  setDimension : function(dimension) {
    this.dimension = dimension;
    return this;
  },
  setPoint : function(x, y) {
    this.x = x;
    this.y = y;
    return this;
  }
};

//Intancias, nomás para ver cómo jala.

var punto0 = new Point(),
    punto1 = new Point('white', 3, 10, 20),

    canvas0 = new Canvas();

canvas0.listen();
